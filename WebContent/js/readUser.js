$( document ).ready(function() {

manageData();

/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: '/Dev/ReadCtrl',
    }).done(function(data){
    	manageRow(data);
    });

}

function manageRow(data) {
	var	rows = '';
	$.each( data, function( key, value ) {
	  	rows = rows + '<tr>';	  
      	rows = rows + '<td>'+value.username +'</td>';
	  	rows = rows + '<td>'+value.password+'</td>';
        rows = rows + '<td>'+value.name+'</td>';
        rows = rows + '<td>'+value.surname+'</td>';
        rows = rows + '<td>'+value.country+'</td>';
        rows = rows + '<td>'+value.email+'</td>';
	  	rows = rows + '<td data-id="'+value.idProduct+'">';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';
        rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
        rows = rows + '</td>';
	  	rows = rows + '</tr>';
	});
	
	

	$("tbody").html(rows);
}

});