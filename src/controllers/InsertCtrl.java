package controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import models.User;

/**
 * Servlet implementation class InsertCtrl
 */
@WebServlet("Controller/InsertCtrl")
public class InsertCtrl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertCtrl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String country = request.getParameter("country");
		String email = request.getParameter("email");
		
		//JSON OBJS
		JSONArray output = new JSONArray();
		JSONObject item = new JSONObject();
		
		//LOAL MODEL
		User x =  new User();
		x.setUsername(username);
		x.setName(name);
		x.setSurname(surname);
		x.setPassword(password);//MD5
		x.setCountry(country);
		x.setEmail(email);
		
		if (x.Insert()){
			item.put("status",true); 	// SEND JSON OK
		}else {
			item.put("status", false); // SEND JSON KO
		}
		output.add(item); 
	
	 	response.getWriter().append(output.toJSONString());   		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
