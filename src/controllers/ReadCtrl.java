package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import models.User;

/**
 * Servlet implementation class ReadCtrl
 */
@WebServlet("/ReadCtrl")
public class ReadCtrl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReadCtrl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		JSONArray output = new JSONArray();
		
	 	try{ 		
	 		
			List<User> users = User.GetUsers();
			
			for (User object: users) {
				
				JSONObject item = new JSONObject();
				item.put("idUser", object.getIdUser());
				item.put("username", object.getUsername());
				item.put("name", object.getName());
				item.put("surname", object.getSurname());
				item.put("coutry", object.getCountry());
				item.put("email", object.getEmail());
				
				output.add(item);  
			}
	 	    
	    }
	    catch(Exception ex)
	    {
	    	JSONObject item = new JSONObject();
	    	item.put("error",ex.getMessage());
	    	output.add(item); 
	    }
	 	
	 	response.getWriter().append(output.toJSONString());
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
