package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

public class User {

	private int idUser;
	private String username;
	private String password;
	private String name;
	private String surname;
	private String country;
	private String email;


	//CONSTRUCTORS
	public User(){
		
	}
	
	
	public static  List<User> GetUsers(){	
		
		 try{
				// create our mysql database connection
				Connection conn=DBconn.getconnection();
				
				// create the java statement
				Statement st = conn.createStatement();
		        
				//SQL STRING
				String sql  = "SELECT * from users";
				
				// execute the query, and get a java resultset
				ResultSet rs = st.executeQuery(sql);
		       
		        //create list product
		        List<User> users = new ArrayList<User>();
		        
		        while(rs.next())
		        {		        
		        	User item = new User();

		        	item.setIdUser(Integer.parseInt(rs.getString("idUser")));
		        	item.setName(rs.getString("name"));
		        	item.setSurname(rs.getString("surname"));
		        	item.setUsername(rs.getString("username"));
		        	item.setEmail(rs.getString("email"));
		        	item.setCountry(rs.getString("country")); 
		        
		        	users.add(item);
		        }
		        
		        //close recordset
		        rs.close();
		        
		        return users;
		        
		    }
		    catch(Exception ex)
		    {
		    	System.out.println(ex.getMessage());
		    	return null;
		    }
	}
	
	
	
	public boolean Delete () {
		try {
			//DELETE SSQL
		    String sql = "DELETE FROM users WHERE idUser ="+ this.idUser;  
		    Connection conn=DBconn.getconnection();
	        PreparedStatement pst = (PreparedStatement) conn.prepareStatement(sql);				
			pst.executeUpdate();
			return true;
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
			
	}
	
	
	public boolean Insert (){
		try{
			String sql = "INSERT INTO Users (name,surname,username,password,country,email) VALUES (?,?,?,md5(?),?,?)";  
			 
		 	Connection conn=DBconn.getconnection();
	        PreparedStatement pst = (PreparedStatement) conn.prepareStatement(sql);
			pst.setString(1, this.name);
			pst.setString(2, this.surname);
			pst.setString(3, this.username);
			pst.setString(4, this.password);
			pst.setString(5, this.country);
			pst.setString(6, this.email);
			pst.executeUpdate();			
            return true;
            
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
			return false;
		}
	}
	
	
	
	
	
	public int getIdUser() {
		return idUser;
	}


	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
